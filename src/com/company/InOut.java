package com.company;

import java.util.Scanner;

public class InOut {
    private static Scanner scan = new Scanner(System.in);
    Carteador carteador = new Carteador();
    private static String inicio;

    public InOut() {
    }

    public void iniciar() {
        do {
            System.out.println("Digite 'iniciar' para iniciar o jogo ou 'sair' para fechar.");
            inicio = scan.next();
            if (inicio.equals("iniciar")) {
                imprimirCarta();
                inicio = "sair";
            }else if(!inicio.equals("sair")){
                System.out.println("Opção Inválida");
            }
        } while (!inicio.equals("sair"));
    }

    public void imprimirCarta() {
        carteador.sortearBaralho();
        System.out.println("As suas cartas foram " + carteador.getCartasJogador());
        System.out.println("A sua pontuação foi: ");
    }
}