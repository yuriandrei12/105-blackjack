package com.company;

import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class Carteador {
    private static Scanner scan = new Scanner(System.in);
    private static String jogada;
    private int pontos;
    private int x;
    ArrayList<String> cartasJogador = new ArrayList<>();
    ArrayList<Pontuacao> pontuacao = new ArrayList<>();

    public void setCartasJogador(ArrayList<String> cartasJogador) {
        this.cartasJogador = cartasJogador;
    }

    public ArrayList<String> getCartasJogador() {
        return cartasJogador;
    }

    public Carteador() {
    }

    public void sortearBaralho() {
        ArrayList<Cartas> baralho = new ArrayList<>();
        ArrayList<String> cartasJogador = new ArrayList<>();
        for (Cartas.Naipe naipe : Cartas.Naipe.values()) {
            for (Cartas.Carta carta : Cartas.Carta.values()) {
                baralho.add(new Cartas(carta, naipe));
            }
        }

        int i = new Random().nextInt(baralho.size());
        cartasJogador.add(baralho.get(i).carta.toString() + " de " + baralho.get(i).naipe.toString());
        //pontuacao.add(Pontuacao.VALETE.getValue());
        baralho.remove(i);
        System.out.println(cartasJogador);
        do {
            System.out.println("Para tirar outra carta digite 'sim' ou digite 'encerrar' para encerrar a jogada.");
            jogada = scan.next();
            if ((!jogada.equals("sim") && (!jogada.equals("encerrar")))) {
                do {
                    System.out.println("Opção Inválida");
                    System.out.println("Para tirar outra carta digite 'sim' ou digite 'encerrar' para encerrar a jogada.");
                    jogada = scan.next();
                } while ((!jogada.equals("sim") && !jogada.equals("encerrar")));
            }
            if (jogada.equals("sim")){
                int n = new Random().nextInt(baralho.size());
                cartasJogador.add(baralho.get(n).carta.toString() + " de " + baralho.get(n).naipe.toString());
                //pontuacao.add(Pontuacao.VALETE.getValue());
                baralho.remove(n);
                System.out.println(cartasJogador);
            }
        } while (!jogada.equals("encerrar") || jogada.equals("sim"));
        for (Pontuacao ponto : pontuacao){
            //int value = (Pontuacao.values()[z]).getValue();
            pontos += ponto.getValue();
            System.out.println(pontos);
        }

        setCartasJogador(cartasJogador);
    }
}