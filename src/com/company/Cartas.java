package com.company;

public class Cartas {
    public enum Carta{
        AS,
        DOIS,
        TRES,
        QUATRO,
        CINCO,
        SEIS,
        SETE,
        OITO,
        NOVE,
        DEZ,
        VALETE,
        DAMA,
        REI
        ;
    }

    public enum Naipe{
        OUROS,
        ESPADAS,
        COPAS,
        PAUS
    }

    public final Carta carta;
    public final Naipe naipe;

    public Cartas(Carta carta, Naipe naipe) {
        this.carta = carta;
        this.naipe = naipe;
    }

    public Carta carta() {
        return carta;
    }

    public Naipe naipe() {
        return naipe;
    }
}